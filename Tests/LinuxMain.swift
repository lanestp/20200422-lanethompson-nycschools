import XCTest

import NYCSchoolDataTests

var tests = [XCTestCaseEntry]()
tests += NYCSchoolDataTests.allTests()
XCTMain(tests)
