import XCTest
@testable import NYCSchoolData

final class NYCSchoolDataTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(NYCSchoolData().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
