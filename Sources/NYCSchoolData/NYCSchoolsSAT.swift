public let NYCSchoolsSATUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

 public struct NYCSchoolsSAT:Codable, Hashable {
  public var sat_critical_reading_avg_score:String?
  public var sat_math_avg_score:String?
  public var dbn:String?
  public var sat_writing_avg_score:String?
  public var school_name:String?
  public var num_of_sat_test_takers:String?
}