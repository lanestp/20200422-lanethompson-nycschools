import Foundation
import Combine

final public class NYCSchoolData: ObservableObject {
  public init(){}
  @Published public private(set) var SchoolsInfo = [NYCSchoolsInfo]()
  @Published public private(set) var SchoolsSAT = [NYCSchoolsSAT]()

  private var GetSchoolsInfo: AnyCancellable?
  private var GetSchoolsSAT: AnyCancellable?

  public func SATForSchoolInfo(info:NYCSchoolsInfo?) -> NYCSchoolsSAT? {
    guard let info = info
    else { return nil }
    
    for school in SchoolsSAT {
      if school.school_name?.lowercased() == info.school_name?.lowercased() {
        return school
      }
    }
    return nil
  }
  
  public func Invoke() {
    self.GetSchoolsInfo = URLSession.shared.dataTaskPublisher(for: URL(string: NYCSchoolsInfoUrl)!)
        .map { $0.data }
        .decode(type: [NYCSchoolsInfo].self, decoder: JSONDecoder())
        .replaceError(with: [])
        .receive(on: RunLoop.main)
        .eraseToAnyPublisher()
        .assign(to: \.SchoolsInfo, on: self)

    self.GetSchoolsSAT = URLSession.shared.dataTaskPublisher(for: URL(string: NYCSchoolsSATUrl)!)
        .map { $0.data }
        .decode(type: [NYCSchoolsSAT].self, decoder: JSONDecoder())
        .replaceError(with: [])
        .receive(on: RunLoop.main)
        .eraseToAnyPublisher()
        .assign(to: \.SchoolsSAT, on: self)
  }
}

